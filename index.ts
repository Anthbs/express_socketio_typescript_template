import * as express from 'express';
import * as cors from 'cors';
import * as socketio from 'socket.io';
import * as http from 'http';

const app = express();
const server = http.createServer(app);
const io = require('socket.io')(server);


app.get('/', function (req, res) {
	let data = {
		hello: 'world'
	};
  	res.json(data);
});

io.on('connection', function (socket) {
  socket.emit('server2client', { hello: 'world' });
  
  socket.on('client2server', function (data) {
    console.log(data);
  });
});
     
server.listen(80, () => {
	console.log('App Started');

	io.emit('server2allClients');
});










